#!/bin/bash

set -eu

# READ $HOST, $BUCKET, S3_ACCESS_KEY, $S3_SECRET_KEY
. "$( dirname -- "$0"; )/.s3-credentials"


s3_upload()
{
  FILE="$1"

  RESOURCE="/${BUCKET}/${FILE}"
  CONTENT_TYPE="application/octet-stream"
  DATE=`date -R`
  REQUEST="PUT\n\n${CONTENT_TYPE}\n${DATE}\n${RESOURCE}"
  SIGNATURE=`echo -en ${REQUEST} | openssl sha1 -hmac ${S3_SECRET_KEY} -binary | base64`

  curl -v -X PUT -T "${FILE}" \
            -H "HOST: $HOST" \
            -H "DATE: ${DATE}" \
            -H "Content-Type: ${CONTENT_TYPE}" \
            -H "Authorization: AWS ${S3_ACCESS_KEY}:${SIGNATURE}" \
            https://$HOST${RESOURCE}
}

upload_directory()
{
  for i in "$1"/* ; do
    if [ -f "$i" ]; then
      s3_upload "$i"
    fi

    if [ -d "$i" ]; then
      upload_directory "$i"
    fi

  done
}


case "$1" in
-d)
  # trim trailing slash if exists
  TARGET_DIR=${2%/}

  upload_directory "$TARGET_DIR" ;;
-f)
  s3_upload "$2" ;;
*)
  echo "$1 is not an option";;
esac
