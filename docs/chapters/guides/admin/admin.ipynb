{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Administator Guide"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This guide is for admins wishing to set up and maintain their own OPeNDAP servers.\n",
    "\n",
    "We have tried to make set-up of servers as hassle-free as possible by Dockerizing everything from the PyDAP layer to the nginx, flask and wsgi layers. \n",
    "\n",
    "This way admins do not have to worry about setting up all the environments and code, a few simple CLI commands do the trick for a basic server setup."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```{important}\n",
    "Admins need to have a running instance for Docker Engine for the instructions below to work. Check the [Docker website](https://docs.docker.com/engine/install/ubuntu/) for guides on installation.\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1. **Installation**\n",
    "\n",
    "- Clone the code repository to your working directory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "git clone git@git.ufz.de:khant/opendap.git"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Build an image of the OPeDAP Data Catalogue using Docker"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "#build docker image:\n",
    "docker build . -t \"catalog\" \n",
    "docker build . -t \"catalog\" --platform=linux/arm64 #--> for Arm based chips (Mac M1/M2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "-  Create containers from the base image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "#start docker container and bind port 80 to 80:\n",
    "docker run -p 80:80 -d catalog:latest #--> (If data is inside the app folder)  \n",
    "docker run -p 80:80 -v /data:/data catalog #--> (only if data folder is in root folder)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2. **Configuration**\n",
    "\n",
    "- Configure your catalog instance using environment variables. Set up the Pydap server by specifying configurations like server settings, dataset paths, and other parameters in the configuration files.\n",
    "\n",
    "```{tip}\n",
    "Environment variables can be defined in a `.env` file in the `app` directory. Or you can set them in your Docker environment.\n",
    "```\n",
    "-  The following environment variables are currently available for configuration, however we expect to add more configuration variables in the future. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "APP_DEBUG (default: False)\n",
    "APP_PATH (default: /data)\n",
    "APP_HOST (default: 0.0.0.0)\n",
    "APP_PORT (default: 80)\n",
    "BASIC_AUTH (default: false)\n",
    "WSGI_AUTH_CREDENTIALS (default: none)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3. **Dataset Preparation**\n",
    "\n",
    "- Prepare datasets: Make the datasets accessible to the Pydap server. This may involve ensuring the datasets are in supported formats and configuring the server to recognize and serve these datasets. The supported data formats are:\n",
    "    1. NetCDF4\n",
    "    2. HDF5\n",
    "    3. CSV, w/ JSON metdata\n",
    "    4. SQL\n",
    "\n",
    "\n",
    "- All the datasets should be placed in the `data` folder, which is inside the `app` folder. If the data folder is in the root folder, then the containers have to be build differently (see Section 1 above).\n",
    "\n",
    "- Admins are free to create their own folder structures in the `data` folder, the catalog does not enforce any rules inside this folder.\n",
    "\n",
    "- Datasets not in the supported formats can also be uploaded, however such datasets will only be available for download rather than as objects. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4. **S3/MinIO Connection** [OPTIONAL]\n",
    "\n",
    "The UFZ has a MinIO instance currently available internally on https://minio.ufz.de:9000/login.\n",
    "\n",
    "```{important}\n",
    " UFZ MinIO is only accessible from within UFZ intranet. Please contact the RDM department for getting an account and resource allocation.\n",
    "\n",
    "```\n",
    "\n",
    "The OPeNDAP Catalog can be configured for a birection data flow from/to MinIO, or any other S3 based object storage (not only specific to UFZ). \n",
    "\n",
    "#### Catalog --> MinIO/S3\n",
    "In order to synchronoze your data in the `app/data` folder with any MinIO/S3 instance:\n",
    "\n",
    "**Minio**\n",
    "\n",
    "- Access to admin-frontend: https://minio.ufz.de:9000/\n",
    "- Login with UFZ-credentials\n",
    "- To work with the scripts, create credentials for S3-API-access under 'Access Keys'\n",
    "  - Create Access- and Secretkey\n",
    "  - (leave 'Restrict beyond user policy' as OFF)\n",
    "\n",
    "\n",
    "**Prerequisites**\n",
    "\n",
    "- Copy `minio/.s3-credentials.example` to a new file called `minio/.s3-credentials`\n",
    "- (the file will be ignored by git)\n",
    "- Insert your credentials into the file\n",
    "\n",
    "**Usage**\n",
    "\n",
    "- To upload all files in a folder recursively (and keeping the directory structure), e.g.:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "./s3-upload.sh -d testdata"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- To upload a single file, e.g.:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "vscode": {
     "languageId": "shellscript"
    }
   },
   "outputs": [],
   "source": [
    "./s3-upload.sh -f testdata/hello-world.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### MinIO/S3 --> Catalog\n",
    "\n",
    "Coming soon"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4. **Security (Authentication)** :\n",
    "\n",
    "The WSGI server is configured to use basic authentication. Simply set the `BASIC_AUTH` environment variable to `true` to enable it. By default, user authentication is disabled.\n",
    "\n",
    "The credentials are set using the `WSGI_AUTH_CREDENTIALS` environment variable. Usernames and password can be addded using the notation `username:password` and multiple users can be added using the notation:\n",
    "\n",
    "```\n",
    "WSGI_AUTH_CREDENTIALS=username:password|username2:password2|...\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5. **Server Deployment**:\n",
    "\n",
    "- Deploy the Pydap server to any system that allows docker to expose ports on the network. By defeault, it is set to bind the container's port 80 to the port 80 of the host machine. "
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "name": "python"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
