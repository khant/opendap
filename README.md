# OPeNDAP Data Catalog

This is a base template for setting up a dockerized pydap installation.

Everything in the `data` folder is served by the flask-nginx-uWSGI server.

### Installation guide

1. Create a `.env` file in the `app` folder if it does not exist and add the following environment variables as needed (none of the env variables are required):

```
  - APP_DEBUG (default: False)
  - APP_PATH (default: /data)
  - APP_HOST (default: 0.0.0.0)
  - APP_PORT (default: 80)
  - BASIC_AUTH (default: False)
  - WSGI_AUTH_CREDENTIALS=username:password|username2:password2|...
```
2. Add the data you want to serve in the `data` folder.

3. Build the docker image using the following command:

```bash
docker build . -t "opendap-server" #--> for x86 based chips
#OR
docker build . -t "opendap-server" --platform=linux/arm64 #--> for Arm based chips (Mac M1/M2)
```
4. Spawn a docker container using one of the following command:

```bash
docker run -p 80:80 -d opendap-server:latest` #--> if data folder is inside the app folder
docker run -p 80:80 --env-file app/.env -v <path/to/data/folder>:/data opendap-server #--> only if data folder is in root folder and APP_PATH envionment variable is set correctly

```

### WSGI Basic Authentication

The WSGI server is configured to use basic authentication. Simply set the `BASIC_AUTH` environment variable to `true` to enable it. By default, user authentication is disabled.

The credentials are set using the `WSGI_AUTH_CREDENTIALS` environment variable. Usernames and password can be added using the notation `username:password` and multiple users can be added using the notation:

```
WSGI_AUTH_CREDENTIALS=username:password|username2:password2|...
```

### Running Documentation

The documentation for the template have been created using the Jupyter Book tool, and can be found under the `docs/` folder.
To run the Jupyter Book:

1. Install dependency

```bash
pip install -U jupyter-book
```

2. Build the book

```bash
jupyter-book build docs/
```

3. Go to `docs/_build/html/` and open index.html in a browser window.

### The DAP Data Model

DAP stands for Data Access Protocol and we are currently using DAP4 in this catalog.

The DAP4 data model can be visualized as follows:

![DAP4](https://docs.opendap.org/images/7/7e/DAP_4_DM_UML.png)

The complete DAP specification can be found here: [https://www.opendap.org/pdf/dap_objects.pdf](https://www.opendap.org/pdf/dap_objects.pdf)
