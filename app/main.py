import os
from flask import Flask
from pydap.wsgi.app import DapServer
import multiprocessing as mp
from wsgi_basic_auth import BasicAuth

num_workers = mp.cpu_count()
print("The current worker count is= ", num_workers)

app_debug = os.environ.get("APP_DEBUG", "False").lower() in ["true", "yes", "y", "1"]
# TODO: move data folder to the root directory so that docker image doesn't need to be created everytime (docker containers will just need to be restarted)
app_path = os.environ.get("APP_PATH", "/app/data")
app_host = os.environ.get("APP_HOST", "0.0.0.0")
app_port = int(os.environ.get("APP_PORT", "80"))
use_auth = os.environ.get("BASIC_AUTH", "false").lower()
print("use_auth=", use_auth)

app = Flask(__name__)
app.wsgi_app = DapServer(app_path)
if use_auth == "true":
    print("Using Basic Auth")
    app.wsgi_app = BasicAuth(app.wsgi_app)

if __name__ == "__main__":
    app.run(host=app_host, debug=app_debug, port=app_port, threaded=True)
